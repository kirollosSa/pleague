//
//  TeamCellViewModel.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

struct TeamCellViewModel: BaseViewModel {
    private let team: Teams
    
    init(team: Teams) {
        self.team = team
    }
    
    var teamName: String {
        if let name = team.name {
            return name
        }
        return ""
    }
    
    var teamColosr: String {
        if let colors = team.clubColors {
            return colors
        }
        return ""
    }
    
    var teamVenue: String {
        if let venue = team.venue {
            return venue
        }
        return ""
    }
    
    var teamWebsiteUrl: URL? {
        if let website = team.website {
            if let url = URL(string: website) {
                return url
            }
        }
        return nil
    }
    
    func canOpenteamWebsite() -> Bool {
        if let url = URL(string: team.website ?? "") {
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
}
