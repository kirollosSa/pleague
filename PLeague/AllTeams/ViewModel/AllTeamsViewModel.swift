//
//  AllTeamsViewModel.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation

class AllTeamsViewModel: StateViewModel {
    // MARK: - Bindables
    private(set) var loaderState = Bindable<LoadingIndicatorState>(.hidden)
    private(set) var teams = Bindable<[Teams]>([])
    private(set) var errorMessage = Bindable<String?>(nil)
    
    //MARK:- iVars
    var teamsCount: Int {
        return teams.value.count
    }
    
    func getItemViewModel(index: Int) -> TeamCellViewModel {
        return TeamCellViewModel(team: teams.value[index])
    }
    
    func getAllTeams() {
        let header = [KeyParamters.authToke:kAuthToken]
        let url = APIs.Teams.getAllTeamsUrl(.pLeague)
        loaderState.value = .overlay
        APIManager.sharedInstance.getRequest(url,header: header) { (response) in
            switch response.result
            {
            case .success( _) :
                self.loaderState.value = .hidden
                do {
                    guard let data = response.data else {
                        return
                    }
                    let teamResponse = try JSONDecoder().decode(TeamsResponse.self, from: data)
                    //TODO:- handle pagination
                    self.teams.value = teamResponse.teams ?? []
                }
                catch {
                    self.errorMessage.value = "Something went wrong"
                }
            case .failure(let error):
                self.loaderState.value = .hidden
                self.errorMessage.value = error.localizedDescription
            }
        }
    }
}
