//
//  TeamCollectionViewCell.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

class TeamCollectionViewCell: BaseCollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTeamName: UILabel!
    @IBOutlet weak var lblTeamColor: UILabel!
    @IBOutlet weak var lblTeamVenue: UILabel!
    @IBOutlet weak var websiteBTN: UIButton!
    
    //MARK:- iVars
    var viewWidth = NSLayoutConstraint()
    private(set) var viewModel: TeamCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        viewWidth = self.contentView.widthAnchor.constraint(equalToConstant: 0)
    }
    
    func setupView() {
        lblTeamName.font = UIFont.boldSystemFont(ofSize: 17)
    }
    
    func setViewWidth(widthSize: CGFloat ){
        self.viewWidth.constant = widthSize
        self.viewWidth.isActive = true
    }
    
    func configure(with viewModel: TeamCellViewModel) {
        self.viewModel = viewModel
        lblTeamName.text = viewModel.teamName
        lblTeamColor.text = viewModel.teamColosr
        lblTeamVenue.text = viewModel.teamVenue
    }

    @IBAction func onTabWebsite(_ sender: Any) {
        if viewModel.canOpenteamWebsite() {
            UIApplication.shared.open(viewModel.teamWebsiteUrl!)
        }
    }
}
