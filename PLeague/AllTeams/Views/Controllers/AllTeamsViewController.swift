//
//  AllTeamsViewController.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

class AllTeamsViewController: BaseViewController,HasViewModel {

    //MARK:- IBOutlet
    @IBOutlet weak var teamsCollectionView: UICollectionView!
    
    //MARK:- ViewModel
    var viewModel: AllTeamsViewModel! = AllTeamsViewModel()
    
    //MARK:- View functions
    override func setupView() {
        super.setupView()
        teamsCollectionView.backgroundColor = .white
    }
    
    override func bindData() {
        super.bindData()
        viewModel.getAllTeams()
    }
    
    override func observeBindables() {
        bindDefaultObservables()
        disposeBag.add(
            viewModel.teams.bind({ [weak self] _ in
                self?.teamsCollectionView.reloadData()
            }))
    }
    
    override func registerNib() {
        super.registerNib()
        teamsCollectionView.registerCell(withCellType: TeamCollectionViewCell.self)
        teamsCollectionView.registerFooterView(withViewType: CollectionFooterLoadingView.self)
    }

}

//MARK:- UICollectionViewDataSource
extension AllTeamsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.teamsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell: TeamCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: viewModel.getItemViewModel(index: indexPath.item))
        cell.setViewWidth(widthSize: self.teamsCollectionView.frame.width)
        return cell
    }

}

//MARK:- UICollectionViewDelegateFlowLayout
extension AllTeamsViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width)
        return CGSize(width: width, height: 120)
    }
}
