//
//  Teams.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation

struct Teams : Codable {
    let id : Int?
    let name : String?
    let shortName : String?
    let venue : String?
    let clubColors : String?
    let website : String?
    
    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case shortName = "shortName"
        case website = "website"
        case clubColors = "clubColors"
        case venue = "venue"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        shortName = try values.decodeIfPresent(String.self, forKey: .shortName)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        clubColors = try values.decodeIfPresent(String.self, forKey: .clubColors)
        venue = try values.decodeIfPresent(String.self, forKey: .venue)
    }

}
