//
//  TeamsResponse.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation

struct TeamsResponse : Codable {
    let teams : [Teams]?

    enum CodingKeys: String, CodingKey {

        case teams = "teams"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        teams = try values.decodeIfPresent([Teams].self, forKey: .teams)
    }

}
