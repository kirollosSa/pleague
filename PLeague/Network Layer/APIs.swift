//
//  APIs.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/8/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation

class APIs {
    
    // MARK:- APIs Call to return urls
    struct Teams {
        static func getAllTeamsUrl(_ competitions: Competitions) -> String {
            return kBaseUrl + "/v2/competitions/\(competitions.value)/teams"
        }
        
        static func getTeamUrl(_ teamID: Int) -> String {
            return kBaseUrl + "/v2/teams/\(teamID)"
        }
    }
}
