//
//  APIManager.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/8/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation
import Alamofire


class APIManager {
    //MARK: Singlton
    class var sharedInstance : APIManager {
        struct Singlton {
            static let instance = APIManager()
        }
        return Singlton.instance
    }
    
    // MARK: general requests
    func getRequest(_ url : String ,header: HTTPHeaders = [:],completionHandler :@escaping (DataResponse<Any>) -> Void) {
        _ = Alamofire.request(url,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            completionHandler(response)
        }
    }
    
    /// Use this to check about internet connection
    static var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
