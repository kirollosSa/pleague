//
//  AppConstants.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/8/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation

var kBaseUrl: String {
    return "https://api.football-data.org"
}

//MARK:- Auth Token Key
var kAuthToken: String  {
    return "f9827180713e435aa3675bdee1b3c7a0"
}

enum Competitions {
    case pLeague
    
    var value : String {
        switch self {
        case .pLeague: return "2021"
        }
    }
}

enum LoadingIndicatorState {
    case shown
    case overlay
    case hidden
}

// MARK: - Views tags
class ViewTag {
    static let overlayView = 1001
    static let loaderView = 1002
    static let refreshControl = 1003
    static let loaderMessage = 1004
}

struct KeyParamters {
    // header
    static let authToke = "X-Auth-Token"
}
