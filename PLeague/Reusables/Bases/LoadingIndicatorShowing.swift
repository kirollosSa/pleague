//
//  LoadingIndicatorShowing.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit
protocol LoadingIndicatorShowing {
    func showLoadingIndicator()
    func showOverlayLoadingIndicator(with backgroundColor: UIColor)
    func hideLoadingIndicator()
}

protocol LoaderShowingManager {
    var loaderState: Bindable<LoadingIndicatorState> { get }
}

// this allows us to show/hide loading indicators on any view
extension UIView: LoadingIndicatorShowing {
    
    private var overlayView: UIView {
        if let view = viewWithTag(ViewTag.overlayView) {
            return view
        }
        let view = UIView()
        view.tag = ViewTag.overlayView
        return view
    }
    private var loader: LoadingIndicator {
        if let loader = viewWithTag(ViewTag.loaderView) as? LoadingIndicator {
            return loader
        }
        let loader = LoadingIndicator()
        loader.tag = ViewTag.loaderView
        return loader
    }

    // MARK: - Loading Indicator Handling
    func showLoadingIndicator() {
        guard !subviews.contains(where: {$0 is LoadingIndicator }) else { return }
        addSubview(loader)
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loader.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loader.heightAnchor.constraint(equalToConstant: 30).isActive = true
        loader.widthAnchor.constraint(equalToConstant: 30).isActive = true
        loader.startAnimating()
    }

    func showOverlayLoadingIndicator(with backgroundColor: UIColor = .white) {
        addSubview(overlayView)
        overlayView.backgroundColor = backgroundColor
        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        overlayView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        overlayView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        overlayView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        overlayView.showLoadingIndicator()
    }

    func hideLoadingIndicator() {
        overlayView.removeFromSuperview()
        loader.stopAnimating()
        loader.removeFromSuperview()
    }
}
