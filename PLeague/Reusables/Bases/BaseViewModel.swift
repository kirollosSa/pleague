//
//  BaseViewModel.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation

protocol BaseViewModel {}
protocol StateViewModel: BaseViewModel, InternetConnectionChecker, LoaderShowingManager {}

protocol InternetConnectionChecker {
    func isConnectedToInternet() -> Bool
}

extension InternetConnectionChecker {
    func isConnectedToInternet() -> Bool {
        return APIManager.isConnectedToInternet
    }
}
