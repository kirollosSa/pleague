//
//  BaseCollectionViewCell.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

protocol UISetup {
    func setupView()
}

protocol Configurable {
    associatedtype ViewModel: BaseViewModel
    func configure(with: ViewModel)
}

extension UICollectionReusableView: NibLoadable, Reuseable {}

typealias BaseCollectionViewCell = UICollectionViewCell & UISetup & Configurable
typealias BaseCollectionReusableView = UICollectionReusableView & UISetup & Configurable
typealias BaseTableViewCell = UITableViewCell & UISetup & Configurable
