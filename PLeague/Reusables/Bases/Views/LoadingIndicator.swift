//
//  LoadingIndicator.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

class LoadingIndicator: UIView, NibLoadable, Reuseable {

    var indicator: UIActivityIndicatorView!
    var containerView: UIView!
    
    init(frame: CGRect = CGRect(x: 0, y: 0, width: 30, height: 30), color: UIColor = .darkGray) {
        super.init(frame: frame)
        self.indicator = UIActivityIndicatorView(frame: self.bounds)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addFitSubview(self.indicator)
        self.translatesAutoresizingMaskIntoConstraints = false
            self.addFitSubview(self.indicator)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func startAnimating() {
        self.indicator.startAnimating()
    }

    func stopAnimating() {
        self.indicator.stopAnimating()
    }
}

