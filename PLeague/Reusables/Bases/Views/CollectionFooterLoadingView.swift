//
//  CollectionFooterLoadingView.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

class CollectionFooterLoadingView: BaseCollectionReusableView {

    var loader: LoadingIndicator = LoadingIndicator()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    func setupView() {}
    
    func configure(with viewModel: LoaderFooterViewModel) {
        if viewModel.isAnimating {
            showLoadingIndicator()
        } else {
            hideLoadingIndicator()
        }
    }
    
}

class LoaderFooterViewModel: BaseViewModel {
    var isAnimating: Bool = false
}
