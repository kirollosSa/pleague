//
//  BaseViewController.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var disposeBag = DisposableBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        registerNib()
        observeBindables()
        bindData()
    }
    
    func observeBindables() {}
    func bindData() {}
    func registerNib() {}
    func setupView() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func push(viewController: UIViewController, animated: Bool = true) {
        if let navigationController = navigationController as? BaseNavigationViewController {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
}
