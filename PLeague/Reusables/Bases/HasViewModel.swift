//
//  HasViewModel.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation
import UIKit

protocol HasViewModel: class {
    associatedtype ViewModel: StateViewModel
    var viewModel: ViewModel! { get set }
    var disposeBag: DisposableBag { get set }
    func bindLoaderObservable()
}

extension HasViewModel {
    
    func bindDefaultObservables() {
        bindLoaderObservable()
    }
}

extension HasViewModel where Self: UIViewController {
    func bindLoaderObservable() {
        disposeBag.add(
            viewModel.loaderState.bindAndFire({ [weak self] state in
                switch state {
                case .hidden:
                    self?.view.hideLoadingIndicator()
                case .shown:
                    self?.view.showLoadingIndicator()
                case .overlay:
                    self?.view.showOverlayLoadingIndicator()
                }
            })
        )
    }
}
