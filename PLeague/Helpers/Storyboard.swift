//
//  Storyboard.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import Foundation
import UIKit

class Storyboard {

    private var instance: UIStoryboard

    init(name: StoryboardName) {
        instance = UIStoryboard(name: name.rawValue.capitalizingFirstLetter(), bundle: nil)
    }

    func instatiate<T: UIViewController>(_: T.Type) -> T where T: StoryboardLoadable {
        guard let viewController = self.instance.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("unable to instantiate ViewController form storyboard")
        }
        return viewController
    }
}

protocol StoryboardLoadable: class {
    static var storyboardIdentifier: String { get }
}

extension StoryboardLoadable where Self: UIViewController {

    static var storyboardIdentifier: String {
        return String(describing: self)
    }

    static func instantiateFromStoryboard(_ storybaordName: StoryboardName) -> Self {
        return Storyboard(name: storybaordName).instatiate(Self.self)
    }
}

///TODO:- this enum should contain all storyboard
public enum StoryboardName: String {
    case main
}
