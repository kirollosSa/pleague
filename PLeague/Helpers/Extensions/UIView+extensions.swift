//
//  UIView+extensions.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

extension UIView {
    func addFitSubview(_ view: UIView) {
           view.translatesAutoresizingMaskIntoConstraints = false
           addSubview(view)
           view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
           view.topAnchor.constraint(equalTo: topAnchor).isActive = true
           view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
           view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
       }
}
