//
//  UICollectionView+extension.swift
//  PLeague
//
//  Created by Kirollos Maged on 6/9/20.
//  Copyright © 2020 Kirollos Maged. All rights reserved.
//

import UIKit

extension UICollectionView {
    func registerCell<T: UICollectionViewCell>(withCellType: T.Type) {
        self.register(UINib(nibName: T.nibName, bundle: nil), forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<Base: BaseCollectionViewCell>(forIndexPath indexPath: IndexPath) -> Base {
        if let cell = self.dequeueReusableCell(withReuseIdentifier: Base.reuseIdentifier, for: indexPath) as? Base {
            cell.setupView()
            return cell
        } else {
            fatalError("CollectionView can not dequeue cell with reusableIdentifier '\(Base.reuseIdentifier)' at indexPath '\(indexPath)'")
        }
    }

    func registerFooterView<T: UIView>(withViewType: T.Type) where T: NibLoadable & Reuseable {
        self.register(UINib(nibName: T.nibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableFooterView<T: BaseCollectionReusableView> (forIndexPath indexPath: IndexPath) -> T {
        if let footer = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T {
            footer.setupView()
            return footer
        } else {
            fatalError("CollectionView can not dequeue footer view with reusableIdentifier '\(T.reuseIdentifier)' at indexPath '\(indexPath)'")
        }
    }

    func registerHeaderView<T: UIView>(withViewType: T.Type) where T: NibLoadable & Reuseable {
        self.register(UINib(nibName: T.nibName, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableHeaderView<T: BaseCollectionReusableView> (forIndexPath indexPath: IndexPath) -> T {
        if let header = self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T {
            header.setupView()
            return header
        } else {
            fatalError("CollectionView can not dequeue header view with reusableIdentifier '\(T.reuseIdentifier)' at indexPath '\(indexPath)'")
        }
    }
}
